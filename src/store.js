import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        todos: [],
        completedTodos: []
    },
    mutations: {
        addTodos (state, todos) {
            this.state.todos = todos;
        }
    },
    getters: {
        completedTodos: state => {
            Object.keys(state.todos).forEach(key => {
                const item = state.todos[key];

                if(item.completed) {
                    console.log(item);
                    state.completedTodos[key] = item;
                }
            });
            return state.completedTodos;
        }
    },
    actions: {}
})
